# Fake News Automation System

## Abstract

Our democracy is in crisis. Attackers use fake news, disinformation, manipulation and online tactics to undermine our society. Especially election. According to a book "targeted", Cambridge Analytica intervened Trump campaign and Brexit, French, Nigeria, Ghana, and Mexico presidential elections using data breach in Facebook. This is not finished. Those who work for their own benefit are still working on the "Big Data" to agitate the public. They analyze us and attack our democracy's mechanism, people's rational thoughts.

## Purpose

We are working on this project to show how dangerous it is.

## Core Idea

![img](./img.png)

Fully automated system from gathering personal information to targeted spread. We categorize people's tendency and select information to manipulate. Our models are highly personalized to understand target's thoughts. After the inspection of model, our system automatically select data to spread or even create one using deep fake.

Our model works differently each person. So it's difficult to detect and easy to change public opinion.

## NOTE

This kinds of technology might be dangerous, so we don't open the source code.

## Reference

https://en.wikipedia.org/wiki/Facebook–Cambridge_Analytica_data_scandal